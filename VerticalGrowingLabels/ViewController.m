//
//  ViewController.m
//  VerticalGrowingLabels
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Bharat Modi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    
    NSArray *arrayDummyText;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    arrayDummyText = [[NSArray alloc] initWithObjects:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
                      @"Lorem Ipsum is simply ",
                      @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer s",
                      @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                      @"Lorem Ipsum is simply dummy text of the printing and typesetting industry.",nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel *labelItem = (UILabel*) [cell viewWithTag:10];
    
    UILabel *labelDescription = (UILabel*) [cell viewWithTag:20];

    if (indexPath.row == 1) {
        labelItem.text = arrayDummyText[0];
        labelDescription.text = arrayDummyText[2];
    }
    
    if (indexPath.row == 2) {
        labelItem.text = arrayDummyText[1];
        labelDescription.text = arrayDummyText[0];
    }
    if (indexPath.row == 3) {
        labelItem.text = arrayDummyText[2];
        labelDescription.text = arrayDummyText[0];
    }
    
    if (indexPath.row == 4) {
        labelItem.text = arrayDummyText[0];
        labelDescription.text = arrayDummyText[3];
    }
    return cell;
}

#pragma mark - UITableView Delegates
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}


@end
