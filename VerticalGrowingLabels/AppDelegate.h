//
//  AppDelegate.h
//  VerticalGrowingLabels
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Bharat Modi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

